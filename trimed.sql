-- SELECT 
SELECT 
  productsc.category_name
  ,COUNT(productsc.id) AS count
  ,MIN(productsc.price) AS min_price
  ,MAX(productsc.price) AS max_price
  ,(SELECT productsc.name ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS product_name_with_max_desc_len
  ,MAX(productsc.product_description_length) AS max_lenght_description_in_category 
  ,(SELECT productsc.description ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS max_descriptions
FROM ( 
  SELECT 
    products.id 
    ,products.category_id
    ,products.price
    ,products.name  
    ,products.description
    ,categories.name AS category_name
    ,LENGTH(products.name) AS products_name_length
    ,LENGTH(products.description) AS product_description_length
    FROM  products 
  LEFT JOIN categories ON products.category_id = categories.id 
) AS productsc GROUP BY productsc.category_id \G


-- TRIMED SELECT 
SELECT 
  productsc.category_name
  ,COUNT(productsc.id) AS count
  ,MIN(productsc.price) AS min_price
  ,MAX(productsc.price) AS max_price
  ,(SELECT productsc.name ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS product_name_with_max_desc_len
  ,MAX(productsc.product_description_length) AS max_lenght_description_in_category 
  ,(SELECT productsc.description ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS max_descriptions
FROM ( 
  SELECT 
    products.id 
    ,products.category_id
    ,products.price
    ,TRIM(REPLACE( products.name, '\n','')) AS name
    ,TRIM(REPLACE( products.description, '\n','')) AS description
    ,categories.name AS category_name
    ,LENGTH( TRIM( REPLACE( products.name, '\n',''))) AS products_name_length
    ,LENGTH( TRIM( REPLACE( products.description, '\n',''))) AS product_description_length
    FROM  products 
  LEFT JOIN categories ON products.category_id = categories.id 
) AS productsc GROUP BY productsc.category_id \G


