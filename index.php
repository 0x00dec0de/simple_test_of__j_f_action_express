<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Happy rainbow page!</title>

  <style>
  ul {
    margin: 0 auto; 
    padding: 0; 
    width: 470px; 
  }

  ul:after {
    content: "";
    display: table;
    clear: both;
  }

  li {
    width:150px; 
    height: 150px;
    background-color: #0f981c;
    list-style: none;
    float: left;
    margin: 5px; 
  }

  li:nth-child(-n+3){
    margin-top: 0; 
  }

  li:nth-child(3n) {
    margin-right: 0; 
  }

  li:nth-last-child(-n+3) {
    margin-bottom: 0;
  }

  li:nth-child(3n-2) {
    margin-left: 0;
  }


  body > h1{ text-align: center }
  pre { width:750px; margin: 0 auto}
  </style>
</head>
<body>
  <ul>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>

  <h1>I'm happy rainbow!</h1>
  <script>
    // REINBOW  
    (function q(){
      document.body.children[1].style.color = 'hsl(' + i + ',100%, 50%)'; 
      if ( i === 0 ) a = true; else if (i == 255) a = false;
      if ( !a ) i--; else if ( a ) i++;
      setTimeout( q, 4000/255 )
    })( i=0, a=true );
  </script>

<pre>
SELECT 
  productsc.category_name
  ,COUNT(productsc.id) AS count
  ,MIN(productsc.price) AS min_price
  ,MAX(productsc.price) AS max_price
  ,(SELECT productsc.name ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS product_name_with_max_desc_len
  ,MAX(productsc.product_description_length) AS max_lenght_description_in_category 
  ,(SELECT productsc.description ORDER BY productsc.product_description_length DESC LIMIT 1 ) AS max_descriptions
FROM ( 
  SELECT 
    products.id 
    ,products.category_id
    ,products.price
    ,products.name  
    ,products.description
    ,categories.name AS category_name
    ,LENGTH(products.name) AS products_name_length
    ,LENGTH(products.description) AS product_description_length
    FROM  products 
  LEFT JOIN categories ON products.category_id = categories.id 
) AS productsc GROUP BY productsc.category_id \G
</pre>

<?php 

$loadForLoad = [[ 'Name'    => 'Trixie', 
                  'Color'   => 'Green', 
                  'Element' => 'Earth', 
                  'Likes'   => 'Flowers' 
                ],[ 
                  'Bar'    => 'foo', 
                  'Name'    => 'Tinkerbell',
                  'Element' => 'Air',
                  'Likes'   => 'Singning', 
                  'Color'   => 'Blue'
                ],[
                  'Name'    => 'Blum', 
                  'Color'   => 'Pink',
                  'Likes'   => 'Dancing', 
                  'Name'    => 'Blum', 
                  'Element' => 'Water',
                  ''        => 'Empty' 
                ]];


class AsciiTable {
  
  private $matrix;   
  private $minSize; 

  public function __construct( Array $matrix ){

    if ( ! is_array( $matrix ))
       throw new Exception('Parameter will be an array!');
    
    if ( !count($matrix) )
       throw new Exception('Array will be containt one or more params!');

    if ( !is_array($matrix[0]))
       throw new Exception('Array mast containt another array!');
    
    //TODO Will be add one more checks for inner an array and this array must be
    //as an associative array. It's will be maybe but is not now. 

    (Array) $this->matrix = $matrix; 
    $this->calculateTableSize(); 
  }

  private function calculateTableSize(){

    foreach( $this->matrix as $arr ){
      foreach( $arr as $key => $val ){

        if( ! isset($this->minSize[$key]))
          $this->minSize[$key] = 0; 

        if ( strlen($val) > $this->minSize[$key] )
          $this->minSize[$key] = strlen($val);

        if ( strlen($key) > $this->minSize[$key] )
          $this->minSize[$key] = strlen($key);
      }
    }

  } 

  private function printTableBorder(){

    foreach( $this->minSize as $columnName => $columnSize ){

      printf("%c", 0x2B);

      for ( $r = (int) $columnSize + 2; $r >= 0; $r-- ) printf("%c", 0x2D ); 

    }

    printf("%c%s", 0x2B, '<br>');
  }
  
  private function printTableHeader(){

      foreach( $this->minSize as $columnName => $columnSize ){

        if ( $columnSize >= strlen($columnName) )
          $columnName .= str_repeat("&nbsp;", $columnSize-strlen($columnName)+1);

        printf("%c%s%s%s", 0x7C, "&nbsp;", '<b>'.$columnName.'</b>', "&nbsp;");
      }
      printf("%c%s", 0x7C, '<br>');
  }

  private function printTableRows(){

    foreach($this->matrix as $element ) {
      foreach( $this->minSize as $columnName => $columnSize ){

        if ( !isset($element[$columnName]) )$element[$columnName] = "";

        if ( $columnSize >= strlen($element[$columnName]) ){
          $completedColumnName = $element[$columnName] 
          . str_repeat("&nbsp;", $columnSize-strlen($element[$columnName])+1 );
        }

        if ( $columnName === 'Color'){

          $colorNameWithClass = 
            sprintf("<span style=\"color: %s\">%s</span>", 
                strtolower($element[$columnName]), $completedColumnName);

          printf("%c%s%s%s", 0x7C, "&nbsp;", $colorNameWithClass, "&nbsp;");
        }else{
          printf("%c%s%s%s", 0x7C, "&nbsp;", $completedColumnName, "&nbsp;");
        }

      }

      printf("%c%s", 0x7C, '<br>');
      $this->printTableBorder(); 

    }
  }

  public function printMatrix(){
    $this->printTableBorder(); 
    $this->printTableHeader(); 
    $this->printTableBorder(); 
    $this->printTableRows();
  }
}


$table = new AsciiTable($loadForLoad);
echo '<pre style="text-align:center">';
$table->printMatrix();
echo '</pre>';


?>
</body>
</html>

